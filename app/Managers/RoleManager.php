<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/13/18
 * Time: 2:43 PM
 */

namespace App\Managers;

use App\Models\Role;

class RoleManager extends BaseManager
{
    public function __construct()
    {
        $this->setModel(new Role());
    }

    public function add($data)
    {
        $this->model->fill($data)->save();
        $this->model->syncPermissions($data['permissions']);
        return $this->getModel();
    }

    public function update($data)
    {
        $this->model->syncPermissions($data['permissions']);
        return $this->model->update($data);
    }

    public function canDelete()
    {
        return $this->getModel()->users()->count() == 0;
    }
}