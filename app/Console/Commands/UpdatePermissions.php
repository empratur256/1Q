<?php

namespace App\Console\Commands;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class UpdatePermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Super admin users permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $permissionGroup = config('permission.resources');
        $permissionIds = [];
        $this->info("Start");
        foreach ($permissionGroup as $key=>$resource) {
            foreach ($resource as $keys=>$action){
                $this->output->write('.');
                $permission = Permission::whereName("{$keys}-$key")->first();
                if(empty($permission)){
                    $permission = Permission::create(['name' => "{$keys}-$key"]);
                }else{
                    $permission->name=  "{$keys}-$key";
                    $permission->save();
                }

                $permissionIds[] = $permission->id;
            }
        }
        /** @var Role $role */
        $role = Role::whereName('super_admin')->first();
        $role->syncPermissions($permissionIds);
        $this->line("");
        $this->info("Finished");
    }
}
