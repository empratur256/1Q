<?php

namespace App\Http\Resources\Users;

use App\Managers\UserManager;
use Illuminate\Http\Resources\Json\JsonResource;

class UsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'email'       => $this->email,
            'mobile'      => $this->mobile,
            'roles'       => RoleResource::collection(UserManager::getRoles($this->id)),
        ];
    }
}
