<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Resources\Users\PermissionResource;
use App\Managers\PermissionsManager;

class PermissionsController extends Controller
{
    /**
     * @var PermissionsManager
     */
    private $manager;

    /**
     * PermissionsController constructor.
     * @param PermissionsManager $manager
     */
    public function __construct(PermissionsManager $manager)
    {
        $this->middleware('permission:view-roles',
            ['only' => ['index']]);
        $this->manager = $manager;
    }

    public function index()
    {
        return PermissionResource::collection($this->manager->getAll());
    }
}
