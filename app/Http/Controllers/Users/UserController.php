<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\MyController;
use App\Http\Requests\Users\AddUserRequest;
use App\Http\Requests\Users\EditUserRequest;
use App\Http\Resources\Users\UsersResource;
use App\Managers\UserManager;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends MyController
{
    protected $resource = 'users';

    /**
     * Display a listing of the resource.
     *
     * @param UserManager $manager
     */
    public function __construct(UserManager $manager)
    {
        parent::__construct();
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return UsersResource::collection($this->manager->get(request('limit', 20), []));
    }


    /**
     * @param AddUserRequest $request
     * @return UsersResource
     */
    public function store(AddUserRequest $request)
    {
        $data = $request->toArray();
        $data['password'] = Hash::make($data['password']);
        return new UsersResource(
            $this->manager->add($data)
        );
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return UsersResource
     */
    public function show($id)
    {
        return new UsersResource(
            $this->manager->find($id)
        );
    }

    /**
     * @param EditUserRequest $request
     * @param $id
     * @return UsersResource
     */
    public function update(EditUserRequest $request, $id)
    {
        $this->manager->find($id);
        $data = $request->toArray();
        if (isset($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        $this->manager->update($data);

        if($request->input('update_roles',null))
        {
            $this->manager->updateRoles($request->update_roles);
        }

        return new UsersResource(
            $this->manager->getModel()
        );
    }
}
