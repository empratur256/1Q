<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\MyController;
use App\Http\Requests\Users\AddRoleRequest;
use App\Http\Requests\Users\EditRoleRequest;
use App\Http\Requests\Users\LoginRequest;
use App\Http\Resources\Users\RoleResource;
use App\Managers\RoleManager;
use Illuminate\Http\Request;

class RoleController extends MyController
{
    protected $resource = 'roles';


    /**
     * RoleController constructor.
     * @param RoleManager $manager
     */
    public function __construct(RoleManager $manager)
    {
        parent::__construct();
        $this->manager = $manager;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $roles = $this->manager->get(request('limit', 20),[]);
        return RoleResource::collection($roles);
    }


    /**
     * Store a newly created resource in storage.
     * @param AddRoleRequest $request
     * @return RoleResource
     */
    public function store(AddRoleRequest $request)
    {
        return new RoleResource(
            $this->manager->add([
                'name'         => $request->name,
                'permissions'  => request('selectedPermissions', [])
            ])
        );
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return RoleResource
     */
    public function show($id)
    {
        return new RoleResource(
            $this->manager->find($id)
        );
    }


    /**
     * Update the specified resource in storage.
     * @param EditRoleRequest $request
     * @param $id
     * @return RoleResource
     */
    public function update(EditRoleRequest $request, $id)
    {
        $this->manager->find($id);
        $this->manager->update([
            'name'         => $request->name,
            'permissions'  => request('selectedPermissions', [])
        ]);
        return new RoleResource(
            $this->manager->getModel()
        );
    }

}
