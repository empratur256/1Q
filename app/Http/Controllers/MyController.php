<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/7/18
 * Time: 2:35 PM
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Config;

class MyController extends Controller
{
    protected $resource = '';
    protected $manager = null;

    public function __construct()
    {
        $permissionGroup = config('permission.resources');
        foreach ($permissionGroup[$this->resource] as $keys => $item) {
            $this->middleware('permission:' . $keys . '-' . $this->resource,
                ['only' => $item]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->manager->find($id);
        if ($this->manager->canDelete()) {
            $this->manager->delete($id);
            return response()->json(true);
        }
        return response()->json(false, 409);
    }

    public function edit($id)
    {
        return $this->show($id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
}