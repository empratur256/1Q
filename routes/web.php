<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['middleware' => 'throttle:10,1'], function () use ($router) {
    $router->group(['middleware' => 'auth:api', 'namespace' => 'Users'], function () use ($router) {
        $router->group(['prefix' => 'users'], function () use ($router) {
            $router->get('/', 'UserController@index');
//            $router->get('/create', 'UserController@create');
            $router->get('/{id}', 'UserController@show');
            $router->get('/{id}/edit', 'UserController@edit');
            $router->post('/', 'UserController@store');
            $router->put('/{id}', 'UserController@update');
            $router->patch('/{id}', 'UserController@update');
            $router->delete('/{id}', 'UserController@destroy');
        });
        $router->group(['prefix' => 'roles'], function () use ($router) {
            $router->get('/', 'RoleController@index');
//            $router->get('/create', 'RoleController@create');
            $router->get('/{id}', 'RoleController@show');
            $router->get('/{id}/edit', 'RoleController@edit');
            $router->post('/', 'RoleController@store');
            $router->put('/{id}', 'RoleController@update');
            $router->patch('/{id}', 'RoleController@update');
            $router->delete('/{id}', 'RoleController@destroy');
        });
        $router->group(['prefix' => 'permissions'], function () use ($router) {
            $router->get('/', 'PermissionsController@index');
        });
    });
    $router->post('login', 'LoginController@login');
});
