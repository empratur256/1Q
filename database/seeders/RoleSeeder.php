<?php
namespace Database\Seeders;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::factory()->create();
        /** @var \App\Models\User $user */
        $user = \App\Models\User::find(1);
        $user->syncRoles($role->id);
        $permissionGroup = config('permission.resources');
        $permissionIds = [];
        foreach ($permissionGroup as $key=>$resource) {
            foreach ($resource as $keys=>$action){
                $permission = Permission::whereName("{$keys}-$key")->first();
                if(empty($permission)){
                    $permission = Permission::create(['name' => "{$keys}-$key"]);
                }else{
                    $permission->name=  "{$keys}-$key";
                    $permission->save();
                }
                $permissionIds[] = $permission->id;
            }
        }
        /** @var Role $role */
        $role = Role::whereName('super_admin')->first();
        $role->syncPermissions($permissionIds);
    }
}
