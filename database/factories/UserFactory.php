<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => 1,
            'name' => $this->faker->name,
            'email' => 'root@root.pc',
            'email_verified' => 1,
            'mobile' => '09168163952',
            'mobile_verified' => 1,
            'password' => Hash::make('123456'), // Hash password,
            'remember_token' => 'wefghhgfdsfghmjhgrewqertytrxcvbn',
        ];
    }
}
